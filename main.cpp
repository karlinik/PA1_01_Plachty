#include <stdio.h>
#include<math.h>
#include<float.h>


using namespace std;

int pocetKusu(double plachta_s,double plachta_d,double latka_s,double latka_d, double prekryv)
{
int sloupec=0, radek=0;

//pocet sloupcu vyplne plachty
    if (latka_s>=plachta_s)
        sloupec=1;
    else if(prekryv>=latka_s)
    {
        return 0;
    }
    else{
    while (plachta_s>0 || fabs(plachta_s)< 1e-5)
    {
        plachta_s=(plachta_s-latka_s);
        if (fabs(plachta_s)< 1e-5 || plachta_s<0)
            sloupec++;
        else{
            plachta_s+=prekryv;
            sloupec++;}
    }
    }
   /* printf("\nsirka: %f\n", plachta_s);
    printf("sloupce: %d\n", sloupec);*/

//pocet radku vyplne plachty
    if (latka_d>=plachta_d)
        radek=1;
    else if(prekryv>=latka_d)
    {
        return 0;
    }
    else{
    while(plachta_d>0 || fabs(plachta_d)< 1e-5)
    {
        plachta_d=(plachta_d-latka_d);
        if (fabs(plachta_d)< 1e-5 || plachta_d<0)
            radek++;
        else{
            plachta_d+=prekryv;
            radek++;
        }
    }
    }

    /*printf("delka: %f\n", plachta_d);
    printf("radky: %d\n", radek);*/
    return sloupec*radek;
}

int main()
{
    double latka_s, latka_d, plachta_s, plachta_d, prekryv;
    int pocet_kusu1, pocet_kusu2;
    //Nacteni rozmeru latky
    printf("Velikost latky:\n");
    if (scanf("%lf %lf", &latka_s, &latka_d)!= 2 || latka_d<=0 || latka_s<=0)
    {
        printf("Nespravny vstup.\n");
        return 0;
    }
    //Nacteni rozmeru plachty
    printf("Velikost plachty:\n");
    if (scanf("%lf %lf", &plachta_s, &plachta_d)!= 2 || latka_d<=0 || latka_s<=0)
    {
        printf("Nespravny vstup.\n");
        return 0;
    }
    if (latka_s>=plachta_s && latka_d>=plachta_d)
        {printf("Pocet kusu latky: 1\n");
        return 0;}
    else{
    //Nacteni prekryvu
    printf("Prekryv:\n");
    if (scanf("%lf", &prekryv)!= 1 || prekryv<0)
    {
        printf("Nespravny vstup.\n");
        return 0;
    }
    }

    pocet_kusu1=pocetKusu(plachta_s, plachta_d, latka_s, latka_d, prekryv);
    pocet_kusu2=pocetKusu(plachta_s, plachta_d, latka_d, latka_s, prekryv);

    /*printf("\npocet kusu 1: %d\n", pocet_kusu1);
    printf("pocet kusu 2: %d\n", pocet_kusu2);*/

    if (pocet_kusu1==0 && pocet_kusu2==0)
        printf("Nelze vyrobit.\n");
    else if(pocet_kusu1==0)
        printf("Pocet kusu latky: %d\n", pocet_kusu2);
    else if(pocet_kusu2==0)
        printf("Pocet kusu latky: %d\n", pocet_kusu1);
    else if (pocet_kusu1<=pocet_kusu2)
        printf("Pocet kusu latky: %d\n", pocet_kusu1);
    else printf("Pocet kusu latky: %d\n", pocet_kusu2);

    return 0;
}
